<?php

// OBS: O valor inteiro/número do produto é a quantidade:
// Ex: 'Pão de forma' => QUANTIDADE

return [
    'janeiro' => [
        'alimentos' => [
            'P�o de forma' => 100,
            'Nutella' => 50,
            'Arroz' => 200,
            'Feij�o' => 150,
        ],
        'limpeza' => [
            'Veja multiuso' => 50,
            'Sabao em po' => 50,
            'Desinfetante' => 100,
        ],
        'higiene_pessoal' => [
            'Creme dental' => 500,
            'Sabonete Protex' => 500,
            'Escova de dente' => 500,
            'Papel Hignico' => 1000,
        ],
    ],
    'marco' => [
        'alimentos' => [
            'Ovos' => 1200,
            'Iogurte' => 500,
            'Pasta de Amendoim' => 500,
        ],
        'limpeza' => [
            'Detergente' => 100,
            'Sabao em po' => 100,
        ],
        'higiene_pessoal' => [
            'Enxaguante bocal' => 500,
        ],
    ],
    'maio' => [
        'alimentos' => [
            'Brocolis' => 1000,
            'Tomate' => 1000,
            'Morango' => 10001,
            'Berinjela' => 100,
            'Pepino' => 100,
            'Arroz integral' => 500,
            'Feij�o' => 500,
            'Fil� de frango' => 500,
            'Leite desnatado' => 2000,
        ],
        'limpeza' => [
            'Diabo verde' => 100,
            'MOP' => 100,
        ],
        'higiene_pessoal' => [
            'Protex' => 500,
            'Papel Hignico' => 500,
            'Shampoo' => 500,
        ],
    ],
    'fevereiro' => [
        'alimentos' => [
            'P�o de forma' => 50,
            'Queijo minas' => 50,
            'Gel�ia de morango' => 250,
        ],
        'limpeza' => [
            'Pano de ch�o' => 300,
            'Rodo' => 301,
        ],
        'higiene_pessoal' => [
            'Creme dental' => 500,
            'Sabonete Dove' => 500,
            'Papel Hignico' => 1000,
        ],
    ],
    'abril' => [
        'alimentos' => [
            'Doritos' => 50,
            'Chocolate ao leit' => 100,
            'Fil� Mignon' => 350,
        ],
        'limpeza' => [
            'Esponja de a�o' => 100,
        ],
        'higiene_pessoal' => [
            'Fio dental' => 500,
            'Escova de dente' => 500,
            'Creme dental' => 500,
        ],
    ],
    'junho' => [
        'alimentos' => [],
        'limpeza' => [],
        'higiene_pessoal' => [],
    ],
];
