<?php
/*
 * Autor: Fernando Kogake
 * Data: 16/03/2019
 * Descri��o: Esta Classe possui m�todos para gera��o de um arquivo CSV com a lista de compras
 *
 */
class CSV {
    /* DECLARANDO VARI�VEIS  */
    private $header     = array();
    private $content    = array();
    
    /* M�TODO PARA CARREGAR O CABE�ALHO  */
    public function cabecalho($array_header){
        $this->header = $array_header;
    }
    
    private function geraCabecalho(){
        return $this->header;
    }
    
    /* M�TODO PARA CARREGAR O CONTE�DO  */
    public function conteudo($array_content){
        $this->content = $array_content;
    }
    
    /* M�TODO PARA MONTAR O ARQUIVO CSV E FORMATAR O TEXTO  */
    private function geraConteudo(){
        $array_retorno = array();
        
        /* VERIFICA SE TEM ALGUM CONTE�DO */
        if (count($this->content) > 0) {
           
            foreach ($this->content as $mes=>$mes_array) {
                $array_temp = array();
                foreach ($mes_array as $categoria=>$categoria_array) {
                    foreach ($categoria_array as $produto=>$valor) {
                        $array_temp[] = ucwords($mes);
                        $array_temp[] = ucwords(str_replace("_"," ",$categoria));
                        $array_temp[] = ucwords($produto);
                        $array_temp[] = (int)$valor;
                        $array_retorno[] = $array_temp;
                        unset($array_temp);
                    }
                }
            }
        }
        return $array_retorno;
    }
    
    /* M�TODO PARA GERAR O ARQUIVO CSV  */
    public function gerarCSV(){
        $header_file = $this->geraCabecalho();
        $content_file = $this->geraConteudo();
        $output = fopen('compras-do-ano.csv', 'w+');
        if (!empty($header_file)) { 
            fputcsv($output, $header_file, ';');
        }
        foreach ($content_file as $value) {
            fputcsv($output, $value, ';');
        }
    }
    
}