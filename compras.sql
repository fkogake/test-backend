-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17-Mar-2019 às 02:59
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `compras`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `categoria` varchar(50) NOT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `categoria`, `data_cadastro`) VALUES
(4, 'Alimentos', '2019-03-16 00:00:00'),
(5, 'Higiene Pessoal', '2019-03-16 00:00:00'),
(6, 'Limpeza', '2019-03-16 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `compras`
--

CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `id_mes` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `compras`
--

INSERT INTO `compras` (`id`, `id_mes`, `id_produto`, `quantidade`, `data_cadastro`) VALUES
(137, 1, 6, 200, '2019-03-16 22:58:16'),
(138, 1, 7, 150, '2019-03-16 22:58:16'),
(139, 1, 3, 100, '2019-03-16 22:58:16'),
(140, 1, 5, 50, '2019-03-16 22:58:16'),
(141, 1, 27, 1000, '2019-03-16 22:58:16'),
(142, 1, 24, 500, '2019-03-16 22:58:16'),
(143, 1, 25, 500, '2019-03-16 22:58:16'),
(144, 1, 26, 500, '2019-03-16 22:58:16'),
(145, 1, 36, 100, '2019-03-16 22:58:16'),
(146, 1, 34, 50, '2019-03-16 22:58:16'),
(147, 1, 35, 50, '2019-03-16 22:58:16'),
(148, 2, 20, 250, '2019-03-16 22:58:16'),
(149, 2, 3, 50, '2019-03-16 22:58:16'),
(150, 2, 19, 50, '2019-03-16 22:58:16'),
(151, 2, 27, 1000, '2019-03-16 22:58:16'),
(152, 2, 24, 500, '2019-03-16 22:58:16'),
(153, 2, 31, 500, '2019-03-16 22:58:16'),
(154, 2, 41, 301, '2019-03-16 22:58:16'),
(155, 2, 40, 300, '2019-03-16 22:58:16'),
(156, 3, 8, 1200, '2019-03-16 22:58:16'),
(157, 3, 9, 500, '2019-03-16 22:58:16'),
(158, 3, 10, 500, '2019-03-16 22:58:16'),
(159, 3, 28, 500, '2019-03-16 22:58:16'),
(160, 3, 37, 100, '2019-03-16 22:58:16'),
(161, 3, 35, 100, '2019-03-16 22:58:16'),
(162, 4, 23, 350, '2019-03-16 22:58:16'),
(163, 4, 22, 100, '2019-03-16 22:58:16'),
(164, 4, 21, 50, '2019-03-16 22:58:16'),
(165, 4, 32, 500, '2019-03-16 22:58:16'),
(166, 4, 26, 500, '2019-03-16 22:58:16'),
(167, 4, 24, 500, '2019-03-16 22:58:17'),
(168, 4, 42, 100, '2019-03-16 22:58:17'),
(169, 5, 13, 10001, '2019-03-16 22:58:17'),
(170, 5, 18, 2000, '2019-03-16 22:58:17'),
(171, 5, 12, 1000, '2019-03-16 22:58:17'),
(172, 5, 11, 1000, '2019-03-16 22:58:17'),
(173, 5, 16, 500, '2019-03-16 22:58:17'),
(174, 5, 7, 500, '2019-03-16 22:58:17'),
(175, 5, 17, 500, '2019-03-16 22:58:17'),
(176, 5, 14, 100, '2019-03-16 22:58:17'),
(177, 5, 15, 100, '2019-03-16 22:58:17'),
(178, 5, 29, 500, '2019-03-16 22:58:17'),
(179, 5, 30, 500, '2019-03-16 22:58:17'),
(180, 5, 27, 500, '2019-03-16 22:58:17'),
(181, 5, 38, 100, '2019-03-16 22:58:17'),
(182, 5, 39, 100, '2019-03-16 22:58:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mes`
--

CREATE TABLE `mes` (
  `id_mes` int(11) NOT NULL,
  `mes` varchar(30) DEFAULT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `mes`
--

INSERT INTO `mes` (`id_mes`, `mes`, `data_cadastro`) VALUES
(1, 'Janeiro', '2019-03-16 00:00:00'),
(2, 'Fevereiro', '2019-03-16 00:00:00'),
(3, 'Marco', '2019-03-16 00:00:00'),
(4, 'Abril', '2019-03-16 00:00:00'),
(5, 'Maio', '2019-03-16 00:00:00'),
(6, 'Junho', '2019-03-16 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id_produto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `produto` varchar(50) NOT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id_produto`, `id_categoria`, `produto`, `data_cadastro`) VALUES
(3, 4, 'Pão de Forma', '2019-03-16 00:00:00'),
(4, 4, 'Biscoito', '2019-03-16 00:00:00'),
(5, 4, 'Nutella', '2019-03-16 00:00:00'),
(6, 4, 'Arroz', '2019-03-16 00:00:00'),
(7, 4, 'Feijão', '2019-03-16 00:00:00'),
(8, 4, 'Ovos', '2019-03-16 00:00:00'),
(9, 4, 'Iogurte', '2019-03-16 00:00:00'),
(10, 4, 'Pasta de Amendoim', '2019-03-16 00:00:00'),
(11, 4, 'Brócolis', '2019-03-16 00:00:00'),
(12, 4, 'Tomate', '2019-03-16 00:00:00'),
(13, 4, 'Morango', '2019-03-16 00:00:00'),
(14, 4, 'Berinjela', '2019-03-16 00:00:00'),
(15, 4, 'Pepino', '2019-03-16 00:00:00'),
(16, 4, 'Arroz Integral', '2019-03-16 00:00:00'),
(17, 4, 'Filé de Frango', '2019-03-16 00:00:00'),
(18, 4, 'Leite desnatado', '2019-03-16 00:00:00'),
(19, 4, 'Queijo Minas', '2019-03-16 00:00:00'),
(20, 4, 'Geléia de Morango', '2019-03-16 00:00:00'),
(21, 4, 'Doritos', '2019-03-16 00:00:00'),
(22, 4, 'Chocolate ao leite', '2019-03-16 00:00:00'),
(23, 4, 'Filé Mignon', '2019-03-16 00:00:00'),
(24, 5, 'Creme Dental', '2019-03-16 00:00:00'),
(25, 5, 'Sabonete Protex', '2019-03-16 00:00:00'),
(26, 5, 'Escova de Dente', '2019-03-16 00:00:00'),
(27, 5, 'Papel Higiënico', '2019-03-16 00:00:00'),
(28, 5, 'Enxaguante Bocal', '2019-03-16 00:00:00'),
(29, 5, 'Protex', '2019-03-16 00:00:00'),
(30, 5, 'Shampoo', '2019-03-16 00:00:00'),
(31, 5, 'Sabonete Dove', '2019-03-16 00:00:00'),
(32, 5, 'Fio Dental', '2019-03-16 00:00:00'),
(34, 6, 'Veja Multiuso', '2019-03-16 00:00:00'),
(35, 6, 'Sabão em pó', '2019-03-16 00:00:00'),
(36, 6, 'Desinfetante', '2019-03-16 00:00:00'),
(37, 6, 'Detergente', '2019-03-16 00:00:00'),
(38, 6, 'Diabo Verde', '2019-03-16 00:00:00'),
(39, 6, 'MOP', '2019-03-16 00:00:00'),
(40, 6, 'Pano de Chão', '2019-03-16 00:00:00'),
(41, 6, 'Rodo', '2019-03-16 00:00:00'),
(42, 6, 'Esponja de Aço', '2019-03-16 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`),
  ADD UNIQUE KEY `id_categoria` (`id_categoria`);

--
-- Indexes for table `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `mes`
--
ALTER TABLE `mes`
  ADD PRIMARY KEY (`id_mes`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id_produto`),
  ADD UNIQUE KEY `id_produto` (`id_produto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT for table `mes`
--
ALTER TABLE `mes`
  MODIFY `id_mes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id_produto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
