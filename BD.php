<?php
/*
 * Autor: Fernando Kogake
 * Data: 16/03/2019
 * Descri��o: Esta Classe possui m�todos para manipula��o do banco de dados
 *
 */
class BD {
    
    /* DECLARANDO VARI�VEIS  */
    protected $host="localhost";
    protected $user="root";
    protected $password="";
    protected $database="compras";
    protected $query;
    protected $link;
    protected $result;
    protected $content;
    protected $mysqli;
    
    /* M�TODO PARA CONEX��O COM O BANCO DE DADOS  */
    public function connect(){
        $this->link = new mysqli($this->host, $this->user, $this->password, $this->database);
        
        // CHECAR CONEX�O
        if (mysqli_connect_error()) {
            die("Erro na Conex�o: " . mysqli_connect_error());
        }
        
    }
    
    /* M�TODO PARA CARREGAR O CONTE�DO  */
    public function conteudo($array_content){
        $this->content = $array_content;
    }
    
    /* M�TODO PARA INSERIR OS DADOS NA TABELA COMPRAS  */
    public function inserirDados(){
        // VERIFICA SE EXISTE O CONTE�DO
        if (count($this->content) > 0) {
            $registros_adicionados = 0;
            
            foreach ($this->content as $mes=>$mes_array) {
                $array_temp = array();
                foreach ($mes_array as $categoria=>$categoria_array) {
                    foreach ($categoria_array as $produto=>$valor) {
                        // VERIFICA SE EXISTE O M�S CADASTRADO NA TABELA MES
                        $id_mes         = $this->verificaDados("mes","mes","str",strtolower($mes));
                        $id_cat         = str_replace("_"," ",$categoria);
                        
                        // VERIFICA SE POSSUI A CATEGORIA CADASTRADA NA TABELA CATEGORIA
                        $id_categoria   = $this->verificaDados("categoria","categoria","str",strtolower($id_cat));
                        
                        // VERIFICA SE POSSUI O PRODUTO CADASTRADO NA TABELA PRODUTO
                        $id_produto     = $this->verificaDados("produto","produto","str",strtolower($produto));
                        $quantidade     = (int)$valor;
                        
                        // SE O MES, A CATEGORIA E O PRODUTO EXISTIREM, VERIFICA SE J� FOI CADASTRADO ESTE REGISTRO NA TABELA COMPRAS PARA EVITAR DUPLICIDADE 
                        if($id_mes > 0 && $id_categoria > 0 && $id_produto > 0){
                            $sql        = "SELECT id FROM compras WHERE id_mes = {$id_mes} AND id_produto = {$id_produto}";
                            $result     = mysqli_query($this->link,$sql);
                           
                            // SE N�O EXISTIR, INSERE O REGISTRO NA TABELA COMPRAS
                            if (mysqli_affected_rows($this->link) == 0) {
                                $sql    = "INSERT INTO compras (id_mes,id_produto,quantidade,data_cadastro) VALUES ({$id_mes},{$id_produto},{$quantidade},NOW())";
                                $result = mysqli_query($this->link,$sql);
                                if (mysqli_affected_rows($this->link) > 0) {
                                    $registros_adicionados++;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $registros_adicionados;
    }
    
    /* M�TODO PARA VERIFICAR SE EXISTE OS DADOS NAS TABELAS, SE EXISTIR, RETORNA O ID  */
    public function verificaDados($tabela="",$campo="",$tipo="",$valor=""){
        if($tipo != ""){
            $value=($tipo == "int" ? (int)$valor : "'".$valor."'");
        } else {
            $value = $valor;
        }
        $id_campo = "id_".$campo;
        $sql = "SELECT {$id_campo},lower({$campo}) FROM {$tabela} WHERE {$campo} = {$value}";
        $result = mysqli_query($this->link,$sql);
        
        if (mysqli_affected_rows($this->link) > 0) {
            while($row = $result->fetch_assoc()) {
                return $row[$id_campo];
            }
        }
    }
}