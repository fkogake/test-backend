<?php
/*
 * Autor: Fernando Kogake
 * Data: 16/03/2019
 * Descri��o: Este script l� uma lista de compras no arquivo "lista-de-compras.php", ordena por meses,
 * depois ordena por categorias e depois ordena pela quantidade de forma decrescente, substitui
 * algumas palavras erradas e para finalizar salva os dados no banco de dados "compras"
 *
 * */

/* Adiciona a lista de compras do arquivo para a vari�vel */
$lista              = include 'lista-de-compras.php';

/* DECLARA��O DE ARRAYS  */
$ordem_meses        = array("janeiro","fevereiro","marco","abril","maio","junho");
$ordem_categorias   = array("alimentos","higiene_pessoal","limpeza");
$palavras_erradas   = array("Papel Hignico", "Brocolis", "Chocolate ao leit", "Sabao em po");
$palavras_corretas  = array("Papel Higi�nico","Br�colis","Chocolate ao Leite","Sab�o em p�");
$titulo             = array('M�s','Categoria','Produto','Quantidade');

/* Adiciona o arquivo Controller com fun��es para ordena��o de arrays */
require_once 'Controller.php';

/*  Adiciona o arquivo BD que possui fun��es para manipula��o do Banco de Dados */
require_once 'BD.php';


$objController = new Controller();
/* Instanciando o Objeto Controller  */
$objController = new Controller();

/* ORDENA��O DOS MESES  */
$ordena_meses = $objController->ordenar_array($lista, $ordem_meses);

for($i=0;$i<count($ordem_meses);$i++){
    /* ORDENA��O DAS CATEGORAS  */
    $categoria_ordenada = $objController->ordenar_array($ordena_meses[$ordem_meses[$i]], $ordem_categorias);
    $ordena_categorias[$ordem_meses[$i]] = $categoria_ordenada;
    
    for($x=0;$x<count($ordem_categorias);$x++){
        /* SUBSTITUINDO PALAVRAS ERRADAS  */
        $palavras_substituidas[$ordem_meses[$i]][$ordem_categorias[$x]] = $objController->substituir_palavras($ordena_categorias[$ordem_meses[$i]][$ordem_categorias[$x]],$palavras_erradas,$palavras_corretas);
        
        /* ORDENANDO A QUANTIDADE DE FORMA DECRESCENTE  */
        arsort($palavras_substituidas[$ordem_meses[$i]][$ordem_categorias[$x]]);
        $lista_ordenada[$ordem_meses[$i]][$ordem_categorias[$x]] = $palavras_substituidas[$ordem_meses[$i]][$ordem_categorias[$x]];
    }
    
}

/* INSTANCIANDO O OBJETO BD  */
$objBD = new BD();

/* CONECTANDO AO BANCO DE DADOS  */
$objBD->connect();

/* CARREGANDO O CONTE�DO  */
$objBD->conteudo($lista_ordenada);

/* INSERINDO NO BANCO DE DADOS  */
$reg_adicionados = $objBD->inserirDados();
echo "Total de Registros adicionados: ".$reg_adicionados;