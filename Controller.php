<?php
/* 
 * Autor: Fernando Kogake
 * Data: 16/03/2019
 * Descri��o: Esta Classe possui m�todos para ordena��o de arrays e substitui��o de palavras
 *
 */
Class Controller {
    /*
     * Descri��o: M�todo para ordena��o de Arrays
     *
     */
    public function ordenar_array($lista,$ordem) {
        for($i=0;$i<count($ordem);$i++){
            if(array_key_exists($ordem[$i],$lista)){
                $novo[$ordem[$i]] = $lista[$ordem[$i]];
                foreach($lista as $key=>$val){
                    if($ordem[$i]==$key){
                        $novo[$key] = $lista[$key];
                    }
                }
            }
        }
        
        return $novo;
    }
    
    /*
     * Descri��o: M�todo para substitui��o de palavras erradas na chave de um array
     *
     */
    public function substituir_palavras($arr, $old, $new) {
        foreach($arr as $key=>$val){
            for($i=0;$i<count($old);$i++){
                if($old[$i]==$key){
                    unset($arr[$key]);
                    $arr[$new[$i]] = $val;
                    
                } 
            }
        }
        return $arr;
    }
    
}